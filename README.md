# Zonal stats on Netcdf - Readme and instructions
by Takuya Iwanaga and Mattia Amadio.

This code applies zonal statistics on all bands in a netCDF.

In the example, we use Italian regions to extract the average of temperature from climatic data.
The code is tested to work on a Windows environment using packages available via Anaconda.

## REQUIREMENTS AND RUN:
Requires the installation of numpy, pandas and rasterio libraries.

> conda install -c conda-forge rasterio pandas numpy

- Edit run_operation.py to specify workspace and source files.
- Edit run_operation.py to change "NOME_REG" with the field of zonal areas.
- Run run_operation.py to conduct the analysis.
